// document - it refers to the whole webpage
// querySelector - it is used to select a specific object (HTMl elements) from the document (webpage)
const txtFirstName = document.querySelector("#txt-first-name");

const spanFullName = document.getElementById("span-full-name");

// document.getElementById
// document.getElementByClass
// document.getElementByTagName

// Whenever a user interacts with in the webpage, this action is considered as an event

// addEventListener - function that takes two arguments
// 1. 'keyup' - string identifying an event
// 2. function that the listener will execute once the specified event is triggered

txtFirstName.addEventListener('keyup', (event) => {
	spanFullName.innerHTML = txtFirstName.value;
	console.log(event.target);
	console.log(event.target.value);
});